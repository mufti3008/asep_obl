<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->model('mdl_main');
	}
	public function index()
	{
		// set the timezone 
		date_default_timezone_set("Asia/Jakarta");

		// def template page
		$template = 'template/tpl_main';

		// preparing the data of page
		$data = array(
			'title'=>'OBL',
			'page'=>'page/home/view_main',
			'data'=>$this->mdl_main->get_data(),
			'utang'=>$this->mdl_main->get_utang(),
			'css'=>array(
				base_url().'assets/css/bootstrap-3.3.7.min.css',
				base_url().'assets/plugin/font-awesome-4.7.0/css/font-awesome.css',
				base_url().'assets/css/select.datatables.css',
				base_url().'assets/css/button.datatables.css',
				base_url().'assets/css/editor.datatables.css',
				base_url().'assets/css/novi.css',
				base_url().'assets/css/style.css',
			),
			'js'=>array(
				base_url().'assets/js/jquery-1.12.4.min.js',
				base_url().'assets/js/bootstrap-3.3.7.min.js',
				base_url().'assets/js/datatables.min.js',
				base_url().'assets/js/bootstrap.datatables.min.js',
				base_url().'assets/js/main.js',
				base_url().'assets/js/pre_date.php',
			),
		);

		// load the view page with the data inside
		$this->load->view($template,$data);
	}

	public function update(){
		// get the id of obl table (obl_id) 
		date_default_timezone_set("Asia/Jakarta");
		$timestamp_now = date('Y-m-d H:i:s');

		$data = $this->mdl_main->get_data();
		$i=0;

		$update_status = 0;
		// update data by spesific $param
		foreach ($data as $row) {
			$id = $row['obl_id'];
			// echo $id;
			$inp_p0_start_id[$i] = isset($_POST['inp_p0_start_id_'.$id]) ? $_POST['inp_p0_start_id_'.$id] : NULL;
			if($inp_p0_start_id[$i]){
				$param = 'p0_start';
				$date_input = $inp_p0_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_p0_end_id[$i] = isset($_POST['inp_p0_end_id_'.$id]) ? $_POST['inp_p0_end_id_'.$id] : NULL;
			if($inp_p0_end_id[$i]){
				$param = 'p0_end';
				$date_input = $inp_p0_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}

			$inp_p2_start_id[$i] = isset($_POST['inp_p2_start_id_'.$id]) ? $_POST['inp_p2_start_id_'.$id] : NULL;
			if($inp_p2_start_id[$i]){
				$param = 'p2_start';
				$date_input = $inp_p2_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_p2_end_id[$i] = isset($_POST['inp_p2_end_id_'.$id]) ? $_POST['inp_p2_end_id_'.$id] : NULL;
			if($inp_p2_end_id[$i]){
				$param = 'p2_end';
				$date_input = $inp_p2_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}

			$inp_p7_start_id[$i] = isset($_POST['inp_p7_start_id_'.$id]) ? $_POST['inp_p7_start_id_'.$id] : NULL;
			if($inp_p7_start_id[$i]){
				$param = 'p7_start';
				$date_input = $inp_p7_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_p7_end_id[$i] = isset($_POST['inp_p7_end_id_'.$id]) ? $_POST['inp_p7_end_id_'.$id] : NULL;
			if($inp_p7_end_id[$i]){
				$param = 'p7_end';
				$date_input = $inp_p7_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_kb_start_id[$i] = isset($_POST['inp_kb_start_id_'.$id]) ? $_POST['inp_kb_start_id_'.$id] : NULL;
			if($inp_kb_start_id[$i]){
				$param = 'kb_start';
				$date_input = $inp_kb_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_kb_end_id[$i] = isset($_POST['inp_kb_end_id_'.$id]) ? $_POST['inp_kb_end_id_'.$id] : NULL;
			if($inp_kb_end_id[$i]){
				$param = 'kb_end';
				$date_input = $inp_kb_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_kl_start_id[$i] = isset($_POST['inp_kl_start_id_'.$id]) ? $_POST['inp_kl_start_id_'.$id] : NULL;
			if($inp_kl_start_id[$i]){
				$param = 'kl_start';
				$date_input = $inp_kl_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_kl_end_id[$i] = isset($_POST['inp_kl_end_id_'.$id]) ? $_POST['inp_kl_end_id_'.$id] : NULL;
			if($inp_kl_end_id[$i]){
				$param = 'kl_end';
				$date_input = $inp_kl_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_asn_start_id[$i] = isset($_POST['inp_asn_start_id_'.$id]) ? $_POST['inp_asn_start_id_'.$id] : NULL;
			if($inp_asn_start_id[$i]){
				$param = 'asn_start';
				$date_input = $inp_asn_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_asn_end_id[$i] = isset($_POST['inp_asn_end_id_'.$id]) ? $_POST['inp_asn_end_id_'.$id] : NULL;
			if($inp_asn_end_id[$i]){
				$param = 'asn_end';
				$date_input = $inp_asn_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_del_start_id[$i] = isset($_POST['inp_del_start_id_'.$row['obl_id']]) ? $_POST['inp_del_start_id_'.$row['obl_id']] : NULL;
			if($inp_del_start_id[$i]){
				$param = 'del_start';
				$date_input = $inp_del_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_del_end_id[$i] = isset($_POST['inp_del_end_id_'.$id]) ? $_POST['inp_del_end_id_'.$id] : NULL;
			if($inp_del_end_id[$i]){
				$param = 'del_end';
				$date_input = $inp_del_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_asu_start_id[$i] = isset($_POST['inp_asu_start_id_'.$row['obl_id']]) ? $_POST['inp_asu_start_id_'.$row['obl_id']] : NULL;
			if($inp_asu_start_id[$i]){
				$param = 'asu_start';
				$date_input = $inp_asu_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_asu_end_id[$i] = isset($_POST['inp_asu_end_id_'.$id]) ? $_POST['inp_asu_end_id_'.$id] : NULL;
			if($inp_asu_end_id[$i]){
				$param = 'asu_end';
				$date_input = $inp_asu_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_komi_start_id[$i] = isset($_POST['inp_komi_start_id_'.$row['obl_id']]) ? $_POST['inp_komi_start_id_'.$row['obl_id']] : NULL;
			if($inp_komi_start_id[$i]){
				$param = 'komi_start';
				$date_input = $inp_komi_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_komi_end_id[$i] = isset($_POST['inp_komi_end_id_'.$id]) ? $_POST['inp_komi_end_id_'.$id] : NULL;
			if($inp_komi_end_id[$i]){
				$param = 'komi_end';
				$date_input = $inp_komi_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_bil_start_id[$i] = isset($_POST['inp_bil_start_id_'.$row['obl_id']]) ? $_POST['inp_bil_start_id_'.$row['obl_id']] : NULL;
			if($inp_bil_start_id[$i]){
				$param = 'bil_start';
				$date_input = $inp_bil_start_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_bil_end_id[$i] = isset($_POST['inp_bil_end_id_'.$id]) ? $_POST['inp_bil_end_id_'.$id] : NULL;
			if($inp_bil_end_id[$i]){
				$param = 'bil_end';
				$date_input = $inp_bil_end_id[$i];
				$update_status = $this->proses_update($id,$date_input,$param);
				break;
			}
			$inp_pay_piutang_id[$i] = isset($_POST['inp_pay_piutang_id_'.$id]) ? $_POST['inp_pay_piutang_id_'.$id] : NULL;
			if($inp_pay_piutang_id[$i]){
				if ($this->validateNumber($inp_pay_piutang_id[$i])) {
					$param_utang = 'utang';
					$param = 'pay_piutang';
					$param2 = 'pay_sisa';
					$value_input = $inp_pay_piutang_id[$i];
					$update_status = $this->proses_update_utang_time_data($id,$value_input,$param);
					$update_status2 = $this->proses_update_utang($id,$value_input,$param_utang,$timestamp_now);
					$value_input = 0 - $value_input;
					$update_status3 = $this->proses_update_utang_time_data($id,$value_input,$param2);
					if($update_status==1&&$update_status2==1&&$update_status3==1){
						$update_status=1;
					} else if ($update_status==0&&$update_status2==1){
						$this->mdl_main->role_back_utang();
						$update_status=0;
					} else {
						$update_status=0;
					}
				} else {
					$update_status = 0;
				}
				break;
			}
			$inp_pay_pembayaran_id[$i] = isset($_POST['inp_pay_pembayaran_id_'.$id]) ? $_POST['inp_pay_pembayaran_id_'.$id] : NULL;
			if($inp_pay_pembayaran_id[$i]){
				if ($this->validateNumber($inp_pay_pembayaran_id[$i])) {
					$param_utang = 'bayar';
					$value_input = $inp_pay_pembayaran_id[$i];
					$update_status2 = $this->proses_update_utang($id,$value_input,$param_utang,$timestamp_now);
					
					$param = 'pay_pembayaran';
					$value_input = $row['pay_pembayaran']+$value_input;
					$update_status = $this->proses_update_utang_time_data($id,$value_input,$param);
					// echo "sini";
					// exit();
					
					$param2 = 'pay_sisa';
					$value_input = $row['pay_piutang']-$value_input;
					$value_input = 0 - $value_input;
					$update_status3 = $this->proses_update_utang_time_data($id,$value_input,$param2);
					if($update_status==1&&$update_status2==1&&$update_status3==1){
						$update_status=1;
					} else if ($update_status==0&&$update_status2==1){
						$this->mdl_main->role_back_utang();
						$update_status=0;
					} else {
						$update_status=0;
					}
				} else {
					$update_status = 0;
				}
				break;
			}
		}

		// redirecting to page home with update status message using javascript via form action
		echo '
		<form id="myForm" action="'.base_url().'" method="post">
			<input type="hidden" name="update_status" value="'.$update_status.'">
		</form>';
		echo '
		<script type="text/javascript">
			document.getElementById("myForm").submit();
		</script>
		';
	}

	// and this is the main proses of updating data
	function proses_update($id,$date_input,$param){
		if($this->validateDate($date_input,'d-m-Y')){
			$date_input = strtotime($date_input);
			$date_input = date('Y-m-d',$date_input);
			$data = array($param=>$date_input);
			$test = $this->mdl_main->update_time($id,$data);
			if($test){
				$update_status=1;
			} else {
				$update_status=0;
			}
		} else {
			$update_status=0;
		}
		return $update_status;
	}

	// and this is the main proses of updating data
	function proses_update_utang($id,$value_input,$param_utang,$timestamp_now){
		$data = array(
			'obl_id'=>$id,
			'tipe'=>$param_utang,
			'value'=>$value_input,
			'timestamp'=>$timestamp_now);
		$test = $this->mdl_main->insert_utang($id,$data);
		if($test){
			$update_status=1;
		} else {
			$update_status=0;
		}
		return $update_status;
	}

	// and this is the main proses of updating data
	function proses_update_utang_time_data($id,$value_input,$param){
		$data = array($param=>$value_input);
		$test = $this->mdl_main->update_time($id,$data);
		if($test){
			$update_status=1;
		} else {
			$update_status=0;
		}
		return $update_status;
	}

	function validateNumber($str){
		if (preg_match('/[^0-9]+$/', $str)) {
			return false;
		} else {
   			$str = (string) $str;
			return ctype_digit($str);
		}
	}

	// this is just a function to validate the date
	function validateDate($date, $format = 'Y-m-d H:i:s') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}
}



