<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdl_main extends CI_Model {

    function get_data(){
    	// $sql = "SELECT * FROM obl a 
    	// 	JOIN priority b ON a.priority_id=b.priority_id 
    	// 	JOIN tipe_project c ON a.tipe_project_id=c.tipe_project_id ";
     //    $query = $this->db->query($sql);
     //    return $query->result_array();
    	$this->db->select('a.*,b.nama as nama_pri,c.nama as nama_pro,d.*');
        $this->db->from('obl AS a');
        $this->db->join('priority AS b','a.priority_id = b.priority_id');
        $this->db->join('tipe_project AS c','a.tipe_project_id = c.tipe_project_id');
        $this->db->join('time_data AS d','a.obl_id = d.obl_id');
        $this->db->order_by("a.obl_id","asc");
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_utang(){
        $this->db->select('a.*,b.*');
        $this->db->from('utang AS a');
        $this->db->join('obl AS b','a.obl_id = b.obl_id');
        $this->db->order_by('a.obl_id','asc');
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_time($id,$data){
		$this->db->where('obl_id', $id);
		return $this->db->update('time_data', $data);
    }

    function insert_utang($id,$data){
        return $this->db->insert('utang', $data);
    }

    function role_back_utang(){
        $query = $this->db->query("SELECT * FROM utang ORDER BY utang_id DESC LIMIT 1");
        $result = $query->result_array();
        $id=0;
        foreach ($result as $key) {
            $id = $key['utang_id'];
        }
        if($id==0){
            return false;
        } else {
            return $this->utang_row_delete($id);
        }
    }
    function utang_row_delete($id)
    {
       $this->db->where('utang_id', $id);
       return $this->db->delete('utang');
    }
}