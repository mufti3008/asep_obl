<?php
$i=0;
foreach ($data as $row) {
	// membuat nama inputan
	$inp_p0_start_id[$i] = 'inp_p0_start_id_'.$row['obl_id'];
	$inp_p0_end_id[$i] = 'inp_p0_end_id_'.$row['obl_id'];
	$inp_p2_start_id[$i] = 'inp_p2_start_id_'.$row['obl_id'];
	$inp_p2_end_id[$i] = 'inp_p2_end_id_'.$row['obl_id'];
	$inp_p7_start_id[$i] = 'inp_p7_start_id_'.$row['obl_id'];
	$inp_p7_end_id[$i] = 'inp_p7_end_id_'.$row['obl_id'];
	$inp_kb_start_id[$i] = 'inp_kb_start_id_'.$row['obl_id'];
	$inp_kb_end_id[$i] = 'inp_kb_end_id_'.$row['obl_id'];
	$inp_kl_start_id[$i] = 'inp_kl_start_id_'.$row['obl_id'];
	$inp_kl_end_id[$i] = 'inp_kl_end_id_'.$row['obl_id'];
	$inp_asn_start_id[$i] = 'inp_asn_start_id_'.$row['obl_id'];
	$inp_asn_end_id[$i] = 'inp_asn_end_id_'.$row['obl_id'];
	$inp_del_start_id[$i] = 'inp_del_start_id_'.$row['obl_id'];
	$inp_del_end_id[$i] = 'inp_del_end_id_'.$row['obl_id'];
	$inp_asu_start_id[$i] = 'inp_asu_start_id_'.$row['obl_id'];
	$inp_asu_end_id[$i] = 'inp_asu_end_id_'.$row['obl_id'];
	$inp_komi_start_id[$i] = 'inp_komi_start_id_'.$row['obl_id'];
	$inp_komi_end_id[$i] = 'inp_komi_end_id_'.$row['obl_id'];
	$inp_bil_start_id[$i] = 'inp_bil_start_id_'.$row['obl_id'];
	$inp_bil_end_id[$i] = 'inp_bil_end_id_'.$row['obl_id'];
	$inp_pay_piutang_id[$i] = 'inp_pay_piutang_id_'.$row['obl_id'];
	$inp_pay_pembayaran_id[$i] = 'inp_pay_pembayaran_id_'.$row['obl_id'];
	// end membuat nama inputan
	
	// membuat input atau span tanggal
	if($row['p0_start']){
		$echo_date = $row['p0_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$p0_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$p0_start[$i] = '<input type="text" class="inpdate" name="'.$inp_p0_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['p0_end']){
		$echo_date = $row['p0_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$p0_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['p0_start']) {
			$p0_end[$i] = '<input type="text" class="inpdate" name="'.$inp_p0_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$p0_end[$i]='<span>fill the start date first</span>';
		}
	}
	
	if($row['p2_start']){
		$echo_date = $row['p2_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$p2_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$p2_start[$i] = '<input type="text" class="inpdate" name="'.$inp_p2_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['p2_end']){
		$echo_date = $row['p2_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$p2_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['p2_start']) {
			$p2_end[$i] = '<input type="text" class="inpdate" name="'.$inp_p2_end_id[$i].'"><input type="hidden" name="id" 	value="'.$row['obl_id'].'">';
		} else {
			$p2_end[$i]='<span>fill the start date first</span>';
		}
	}

	if($row['p7_start']){
		$echo_date = $row['p7_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$p7_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$p7_start[$i] = '<input type="text" class="inpdate" name="'.$inp_p7_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['p7_end']){
		$echo_date = $row['p7_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$p7_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['p7_start']) {
			$p7_end[$i] = '<input type="text" class="inpdate" name="'.$inp_p7_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$p7_end[$i]='<span>fill the start date first</span>';
		}
	}

	if($row['kb_start']){
		$echo_date = $row['kb_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$kb_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$kb_start[$i] = '<input type="text" class="inpdate" name="'.$inp_kb_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';

	}

	if($row['kb_end']){
		$echo_date = $row['kb_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$kb_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['kb_start']) {
			$kb_end[$i] = '<input type="text" class="inpdate" name="'.$inp_kb_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$kb_end[$i]='<span>fill the start date first</span>';
		}

	}

	if($row['kl_start']){
		$echo_date = $row['kl_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$kl_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$kl_start[$i] = '<input type="text" class="inpdate" name="'.$inp_kl_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['kl_end']){
		$echo_date = $row['kl_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$kl_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['kl_start']) {
			$kl_end[$i] = '<input type="text" class="inpdate" name="'.$inp_kl_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$kl_end[$i]='<span>fill the start date first</span>';
		}
	}

	if($row['asn_start']){
		$echo_date = $row['asn_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$asn_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$asn_start[$i] = '<input type="text" class="inpdate" name="'.$inp_asn_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['asn_end']){
		$echo_date = $row['asn_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$asn_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['asn_start']) {
			$asn_end[$i] = '<input type="text" class="inpdate" name="'.$inp_asn_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$asn_end[$i]='<span>fill the start date first</span>';
		}
	}

	if($row['del_start']){
		$echo_date = $row['del_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$del_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$del_start[$i] = '<input type="text" class="inpdate" name="'.$inp_del_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['del_end']){
		$echo_date = $row['del_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$del_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['del_start']) {
			$del_end[$i] = '<input type="text" class="inpdate" name="'.$inp_del_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$del_end[$i]='<span>fill the start date first</span>';
		}
	}
	if($row['asu_start']){
		$echo_date = $row['asu_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$asu_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$asu_start[$i] = '<input type="text" class="inpdate" name="'.$inp_asu_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['asu_end']){
		$echo_date = $row['asu_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$asu_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['asu_start']) {
			$asu_end[$i] = '<input type="text" class="inpdate" name="'.$inp_asu_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$asu_end[$i]='<span>fill the start date first</span>';
		}
	}
	if($row['komi_start']){
		$echo_date = $row['komi_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$komi_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$komi_start[$i] = '<input type="text" class="inpdate" name="'.$inp_komi_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['komi_end']){
		$echo_date = $row['komi_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$komi_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['komi_start']) {
			$komi_end[$i] = '<input type="text" class="inpdate" name="'.$inp_komi_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$komi_end[$i]='<span>fill the start date first</span>';
		}
	}
	if($row['bil_start']){
		$echo_date = $row['bil_start'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$bil_start[$i] = '<span>'.$echo_date.'</span>';
	} else {
		$bil_start[$i] = '<input type="text" class="inpdate" name="'.$inp_bil_start_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['bil_end']){
		$echo_date = $row['bil_end'];
		$echo_date = date("d M Y", strtotime($echo_date));
		$bil_end[$i] = '<span>'.$echo_date.'</span>';
	} else {
		if ($row['bil_start']) {
			$bil_end[$i] = '<input type="text" class="inpdate" name="'.$inp_bil_end_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$bil_end[$i]='<span>fill the start date first</span>';
		}
	}

	// if($utang){
	// 	// foreach ($utang as $key) {
	// 	// 	if($key['tipe']=='utang'&&$key['obl_id']==$row['obl_id']){
	// 	// 		$pay_piutang[$i] = $key['value'];
	// 	// 	} else if($key['tipe']=='bayar'&&$key['obl_id']==$row['obl_id']){
	// 	// 		$pay_piutang[$i] = '<input type="text" class="inptext" name="'.$inp_pay_piutang_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	// 	// 	} else {
	// 	// 		$pay_piutang[$i] = '<input type="text" class="inptext" name="'.$inp_pay_piutang_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	// 	// 		$pay_pembayaran[$i]='<span>fill the piutang first</span>';
	// 	// 		$pay_sisa[$i]='';
	// 	// 	}
			
	// 	// 		$bayar = $key['value'];
	// 	// 	} else {
	// 	// 		$pay_pembayaran[$i] = '<input type="text" class="inptext" name="'.$inp_pay_piutang_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	// 	// 	}
	// 	// }
	// } else {
	// 	$pay_piutang[$i] = '<input type="text" class="inptext" name="'.$inp_pay_piutang_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	// 	$pay_pembayaran[$i]='<span>fill the piutang first</span>';
	// 	$pay_sisa[$i]='';
	// }
	// foreach ($utang as $row2) {
	// 	// if()
	// }
	if($row['pay_piutang']){
		$echo_value = $row['pay_piutang'];
		$pay_piutang[$i] = '<span>'.$echo_value.'</span>';
	} else {
		$pay_piutang[$i] = '<input type="text" class="inptext" name="'.$inp_pay_piutang_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
	}

	if($row['pay_pembayaran']>=$row['pay_piutang']){
		$echo_value = $row['pay_pembayaran'];
		$pay_pembayaran[$i] = '<span>'.$echo_value.'</span>';
	} else {
		if ($row['pay_piutang']) {
			$pay_pembayaran[$i] = '<input type="text" class="inptext" name="'.$inp_pay_pembayaran_id[$i].'"><input type="hidden" name="id" value="'.$row['obl_id'].'">';
		} else {
			$pay_pembayaran[$i]='<span>fill the piutang first</span>';
		}
	}

	if($row['pay_sisa']){
		$echo_value = $row['pay_sisa']; 
		if ($row['pay_pembayaran']<$row['pay_piutang']) {
			$pay_sisa[$i] = '<span style="color:#FFD600">'.$echo_value.'</span>';
		} else if ($row['pay_pembayaran']==$row['pay_piutang']){
			$pay_sisa[$i] = '<span style="color:#43A047">lunas</span>';
		} else if ($row['pay_pembayaran']>=$row['pay_piutang']){
			$echo_value = $row['pay_sisa'];
			$pay_sisa[$i] = '<span style="color:#388E3C">+'.$echo_value.'</span>';
		} else {
			$pay_sisa[$i] = '<span>'.$echo_value.'</span>';
		}
	} else {
		if ($row['pay_sisa']>=0 &&$row['pay_sisa']!=null){
			$pay_sisa[$i] = '<span style="color:#43A047">lunas</span>';
		} else {
			$pay_sisa[$i] = '';
		}
	}

	// counter i
	$i++;
}
?>
			<div>
				<div class="col-md-12">
				<div style="max-height: 70vh;" class="table-responsive">
					<table id="table-timedata" class="table table-hover table-bordered table-condensed table-info">
						<thead>
							<tr class="font tr-head" align="center">
								<td align="center" style="vertical-align: middle;" rowspan="2"><b>ID</b></td>
								<td align="center" style="vertical-align: middle;" rowspan="2" nowrap><b>BC NAME </b></td>
								<td align="center" colspan="2"><b>P0-P1 (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>P2-P6 (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>KB (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>ASN (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>P7-P8 (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>KL (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>DELIVERY (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>ASURANSI (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>KOMITE (dd-mm-yyyy)</b></td>
								<td align="center" colspan="2"><b>BILLCOM (dd-mm-yyyy)</b></td>
								<td align="center" colspan="3" nowrap><b>PAYMENT (Rp)</b></td>
							</tr>
							<tr class="tr-head-2" align="center" style="color: white">
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center">Start</td>
								<td align="center">End</td>
								<td align="center" nowrap>PIUTANG</td>
								<td align="center" nowrap>PEMBAYARAN</td>
								<td align="center" nowrap>SISA HUTANG</td>
							</tr>
						</thead>
						<tbody>
						<?php
							$i=0;
							foreach ($data as $row){
								echo '
							<tr class="tr-body">
								<td class="">'.$row['obl_id'].'</td>
								<td class="" nowrap>'.$row['nama_cust'].'</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$p0_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$p0_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$p2_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$p2_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$kb_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$kb_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$asn_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$asn_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$p7_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$p7_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$kl_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$kl_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$del_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$del_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$asu_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$asu_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$komi_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$komi_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$bil_start[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$bil_end[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$pay_piutang[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$pay_pembayaran[$i].'</form>
								</td>
								<td align="center" nowrap>
									<form action="'.base_url().'index.php/home/update" method="post">'.$pay_sisa[$i].'</form>
								</td>
							</tr>
								';
								$i++;
							}
						?>
						</tbody>
					</table>
				</div>
				</div>
			</div>
			
			<div class="clearfix"></div>    