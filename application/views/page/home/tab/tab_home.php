<?php 

// get date time in today time in OOP mode
$date_now_proses = new DateTime('today');
// format() allowing to give return with format you want
$date_now = $date_now_proses->format('d F Y');

// kodingan asep
$nilai=array("141","288","128","96","146","135","916","185","671","186","186","187","135","105","107","70","29","38","56","27","45","31","95","108","142");
$jumlah=0;
$cnt_nilai=count($nilai);
for($k=0;$k<$cnt_nilai;$k++){
	$jumlah=$nilai[$k]+$jumlah;
}
$rata = $jumlah/$cnt_nilai;
$jumlah = "Jumlah = ".$jumlah;
$rata = "AVG = ".$rata;

$nilai1=array("a"=>8.760,"b"=>10.818,"c"=>6.210,"d"=>512,"e"=>3.636,"f"=>34.432,"g"=>23.200,"h"=>1.650,"i"=>909,"j"=>4.450,"k"=>4.108,"l"=>6.522,"m"=>23.428,"n"=>1.192,"o"=>1.592,"p"=>7.247,"q"=>4.734,"r"=>6.614,"s"=>19.909,"t"=>2.266,"u"=>6.460,"v"=>20.719,"w"=>21.718,"x"=>6.565,"y"=>2.995);
$total = 'Total = '.array_sum($nilai1);
// end kodingan asep
?>
			<div>
<?php 
// untuk menampilkan pesan pemberitahuan update
if(isset($_POST['update_status'])){
	if($_POST['update_status']){
		echo '
<div class="alert alert-success alert-dismissible fade in" role="alert">
  <span class="fa fa-check" aria-hidden="true"></span>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <span class="sr-only">Success:</span>
  The data had been updated successfuly.
</div>
		';
	} else {
		echo '
<div class="alert alert-danger alert-dismissible fade in" role="alert">
  <span class="fa fa-exclamation-triangle" aria-hidden="true"></span>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <span class="sr-only">Error:</span>
  Update failed! Please enter valid value (date, number or text) to process the update.
</div>
		';
	}
}

// end untuk menampilkan pesan pemberitahuan update
?>


				<div class="col-md-3 col-sm-6"><span><?php echo $date_now;?></span></div>
				<div class="col-md-3 col-sm-6"><span><?php echo $total;?></span></div>
				<div class="col-md-3 col-sm-6"><span class="pull-right"><?php echo $jumlah;?></span></div>
				<div class="col-md-3 col-sm-6"><span class="pull-right"><?php echo $rata;?></span></div>
				<div class="clearfix"></div>
				<br>
				<div class="col-md-12">
				<div style="max-height: 64vh;" class="table-responsive">
					<table id="table-index" class="table table-hover table-bordered table-condensed table-info">
						<thead>
							<tr class="font" style="color:black" align="center">
								<th class="t2" style="vertical-align: middle;">ID</th>
								<th class="t2" style="vertical-align: middle;">NAMA CUSTOMER</th>
								<th class="t2" style="vertical-align: middle;" nowrap="1" >JUDUL KB</th>
								<th class="t2 dc-nv1" style="font-size: 12px;vertical-align: middle;">PRIORITY</th>
								<th class="t1 dc-nv1" style="vertical-align: middle;">WITEL</th>
								<th class="t1 dc-nv1" style="vertical-align: middle;">MITRA</th>
								<th class="t1">ANGKA CPE</th>
								<th class="t1">TIPE PROJECT</th>
								<th class="t3">START DATE</th>
								<th class="t2 dc-nv1">KOM ITE</th>
								<th class="t2 dc-nv1">P0<br>P1</th>
								<th class="t2 dc-nv3">P2 P6</th>
								<th class="t2 dc-nv5" style="vertical-align: middle;">KB</th>
								<th class="t2 dc-nv1">ASUR<br>ANSI</td>
								<th class="t2 dc-nv4">P7 P8</th>
								<th class="t2 dc-nv4">KL</th>
								<th class="t2 dc-nv1">DELI VERY</th>
								<th class="t2 dc-nv1">BILL COM</th>
								<th class="t2 dc-nv1">PAY MENT</th>
								<th class="t3 dc-nv1">DUE DATE</th>
								<th class="t2 dc-nv1">PRO GRESS</th>
								<th class="t2 dc-nv1" style="vertical-align: middle;">LEFT</th>
								<th class="t2 dc-nv1" style="vertical-align: middle;">TOTAL</th>
								<th class="t2 dc-nv1">ISSUE<br> OBL/DELIVERY</th>
								<th class="t1 dc-nv1">SUDAH DIAKUI</th>
								<th class="t1 dc-nv1">BELUM DIAKUI</th>
								<th class="t1 dc-nv1" style="vertical-align: middle;">PRODUCT SPESIFICATION</th>
							</tr>
						</thead>
						<tbody>
						<?php
						foreach ($data as $row) {
							$start_date = $row['start_date'];
							if($start_date!='0000-00-00'){
								$start_date = date("d M Y", strtotime($start_date));
							} else {
								$start_date='';
							}
							$due_date = $row['due_date'];
							if($due_date!='0000-00-00'){
								$due_date = date("d M Y", strtotime($due_date));

								$p0_start = $row['p0_start'];
								$p0_end = $row['p0_end'];
								if ($p0_start!=NULL&&$p0_end==NULL) {
									$class_status_p0 = 'warning';
								} else if ($p0_start!=NULL&&$p0_end!=NULL) {
									if($row['due_date']>=$row['p0_end']){
										$class_status_p0 = 'success';
									} else {
										$class_status_p0 = 'danger';
									}
								} else {
									$class_status_p0 = 'danger';
								}
								$p2_start = $row['p2_start'];
								$p2_end = $row['p2_end'];
								if ($p2_start!=NULL&&$p2_end==NULL) {
									$class_status_p2 = 'warning';
								} else if ($p2_start!=NULL&&$p2_end!=NULL) {
									$class_status_p2 = 'success';
								} else {
									$class_status_p2 = 'danger';
								}
								$p7_start = $row['p7_start'];
								$p7_end = $row['p7_end'];
								if ($p7_start!=NULL&&$p7_end==NULL) {
									$class_status_p7 = 'warning';
								} else if ($p7_start!=NULL&&$p7_end!=NULL) {
									$class_status_p7 = 'success';
								} else {
									$class_status_p7 = 'danger';
								}
								$kb_start = $row['kb_start'];
								$kb_end = $row['kb_end'];
								if ($kb_start!=NULL&&$kb_end==NULL) {
									$class_status_kb = 'warning';
								} else if ($kb_start!=NULL&&$kb_end!=NULL) {
									$class_status_kb = 'success';
								} else {
									$class_status_kb = 'danger';
								}
								$kl_start = $row['kl_start'];
								$kl_end = $row['kl_end'];
								if ($kl_start!=NULL&&$kl_end==NULL) {
									$class_status_kl = 'warning';
								} else if ($kl_start!=NULL&&$kl_end!=NULL) {
									$class_status_kl = 'success';
								} else {
									$class_status_kl = 'danger';
								}
								$del_start = $row['del_start'];
								$del_end = $row['del_end'];
								if ($del_start!=NULL&&$del_end==NULL) {
									$class_status_del = 'warning';
								} else if ($del_start!=NULL&&$del_end!=NULL) {
									$class_status_del = 'success';
								} else {
									$class_status_del = 'danger';
								}
								$asn_start = $row['asn_start'];
								$asn_end = $row['asn_end'];
								if ($asn_start!=NULL&&$asn_end==NULL) {
									$class_status_asn = 'warning';
								} else if ($asn_start!=NULL&&$asn_end!=NULL) {
									$class_status_asn = 'success';
								} else {
									$class_status_asn = 'danger';
								}
								$asu_start = $row['asu_start'];
								$asu_end = $row['asu_end'];
								if ($asu_start!=NULL&&$asu_end==NULL) {
									$class_status_asu = 'warning';
								} else if ($asu_start!=NULL&&$asu_end!=NULL) {
									$class_status_asu = 'success';
								} else {
									$class_status_asu = 'danger';
								}
								$bil_start = $row['bil_start'];
								$bil_end = $row['bil_end'];
								if ($bil_start!=NULL&&$bil_end==NULL) {
									$class_status_bil = 'warning';
								} else if ($bil_start!=NULL&&$bil_end!=NULL) {
									$class_status_bil = 'success';
								} else {
									$class_status_bil = 'danger';
								}
								$komi_start = $row['komi_start'];
								$komi_end = $row['komi_end'];
								if ($komi_start!=NULL&&$komi_end==NULL) {
									$class_status_komi = 'warning';
								} else if ($komi_start!=NULL&&$komi_end!=NULL) {
									$class_status_komi = 'success';
								} else {
									$class_status_komi = 'danger';
								}
								$class_status_pay = '" style="background:none';
								// $pay_sisa = $row['pay_sisa'];
								// $pay_piutang = 0 - $row['pay_piutang'];
								// if($pay_piutang!=NULL&&$pay_sisa==$pay_piutang){
								// 	$class_status_pay = 'danger';
								// } else if {
									
								// }
							} else {
								$due_date = '';
								$class_status_all = '" style="background:grey';
								$class_status_p0 = $class_status_all;
								$class_status_p2 = $class_status_all;
								$class_status_p7 = $class_status_all;
								$class_status_kb = $class_status_all;
								$class_status_kl = $class_status_all;
								$class_status_komi = $class_status_all;
								$class_status_asu = $class_status_all;
								$class_status_bil = $class_status_all;
								$class_status_del = $class_status_all;
								$class_status_asn = $class_status_all;
								$class_status_p0 = $class_status_all;
								$class_status_p2 = $class_status_all;
								$class_status_p7 = $class_status_all;
								$class_status_kb = $class_status_all;
								$class_status_kl = $class_status_all;
								$class_status_del = $class_status_all;
								$class_status_asn = $class_status_all;
								$class_status_pay = $class_status_all;
							}
							$p0_p1 = $row['p0_p1'];
							if($p0_p1==0){
								$p0_p1='';
							}
							$p2_p6 = $row['p2_p6'];
							if($p2_p6==0){
								$p2_p6='';
							}
							$p7_p8 = $row['p7_p8'];
							if($p7_p8==0){
								$p7_p8='';
							}
							$kb = $row['kb'];
							if($kb==0){
								$kb='';
							}
							$kl = $row['kl'];
							if($kl==0){
								$kl='';
							}
							$delivery = $row['delivery'];
							if($delivery==0){
								$delivery='';
							}
							$payment = $row['payment'];
							if($payment==0){
								$payment='';
							}
							$leftt = $row['leftt'];
							if($leftt==0){
								$leftt='';
							}
							$total = $row['total'];
							if($total==0){
								$total='';
							}
							$sudah_diakui = $row['sudah_diakui'];
							if($sudah_diakui==0){
								$sudah_diakui='';
							}
							$belum_diakui = $row['belum_diakui'];
							if($belum_diakui==0){
								$belum_diakui='';
							}
							echo 
							'<tr class="tr-body">
								<td nowrap="2" class="dc-nv1"><span id="">'.$row['obl_id'].'</span></td>
								<td nowrap="2" class="dc-nv1"><span id="">'.$row['nama_cust'].'</span></td>
								<td nowrap="2" class="dc-nv1">'.$row['judul_kb'].'</td>
								<td class="dc-nv1" align="center">'.$row['nama_pri'].'</td>
								<td class="dc-nv1" align="center">'.$row['witel'].'</td>
								<td align="center" class="dc-nv1" >'.$row['mitra'].'</td>
								<td align="right" class="dc-nv1">'.$row['nilai_cpe'].'</td>
								<td class="dc-nv1" align="center">'.$row['nama_pro'].'</td>
								<td class="dc-nv1" nowrap="2" align="center">'.$start_date.'</td>
								<td class="dc-nv1 '.$class_status_komi.'" align="center">'.$row['komite'].'</td>
								<td class="dc-nv1 '.$class_status_p0.'" align="center" >'.$p0_p1.'</td>
								<td class="dc-nv1 '.$class_status_p2.'" align="center" >'.$p2_p6.'</td>
								<td class="dc-nv1 '.$class_status_kb.'" align="center" >'.$kb.'</td>
								<td class="dc-nv1 '.$class_status_asu.'" align="center" >'.$row['asuransi'].'</td>
								<td class="dc-nv1 '.$class_status_p7.'" align="center">'.$p7_p8.'</td>
								<td class="dc-nv1 '.$class_status_kl.'" align="center">'.$kl.'</td>
								<td class="dc-nv1 '.$class_status_del.'" align="center">'.$delivery.'</td>
								<td class="dc-nv1 '.$class_status_bil.'" align="center">'.$row['billcom'].'</td>
								<td class="dc-nv1 '.$class_status_pay.'" align="center">'.$payment.'</td>
								<td class="dc-nv1" align="center" nowrap>'.$due_date.'</td>
								<td class="dc-nv1" align="center">'.$row['progress'].'</td>
								<td class="dc-nv1" align="center" >'.$leftt.'</td>
								<td class="dc-nv1" align="center" >'.$total.'</td>
								<td class="dc-nv1" nowrap="2" >'.$row['issue_obl_delivery'].'</td>
								<td class="dc-nv1" align="right">'.$sudah_diakui.'</td>
								<td class="dc-nv1" align="right" >'.$belum_diakui.'</td>
								<td class="dc-nv1" nowrap="2" >'.$row['product_spec'].'</td>
							</tr>';
						}?>
							
						</tbody>
					</table>
				</div>
				</div>
			</div>

			<div class="clearfix"></div>    