<section>

<!-- Nav tab -->
<div class="col-md-12">
	<div class="card">
	<center>
		<h3 style="padding-top: 20px;padding-bottom: 15px; background-color: #FF9122; color: white">OBL CPE</h3>
	</center>
	<div class="col-md-12">
		<ul class="nav nav-tabs" role="tablist">
		    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Index</a></li>
		    <li role="presentation"><a href="#timedata" aria-controls="timedata" role="tab" data-toggle="tab">Time Data</a></li>
		</ul>
	</div>
	<!-- Tab panes -->
	<div class="tab-content" style="padding-top: 50px;padding-bottom: 15px;">
	    <div role="tabpanel" class="tab-pane active fade in" id="home">
	    	<?php $this->load->view('page/home/tab/tab_home');?>
	    </div>
	    <div role="tabpanel" class="tab-pane fade" id="timedata">
	    	<?php $this->load->view('page/home/tab/tab_time_data');?>
	    </div>
	</div>
	</div>
</div>
<script type="text/javascript"></script>
</div>
</section>

