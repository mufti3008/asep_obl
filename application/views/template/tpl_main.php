<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title;?></title>

	<!-- load css -->
	<?php 
	if(isset($css) && $css != null){
    	foreach ($css as $row) {
        	echo '<link href="'.$row.'" rel="stylesheet" type="text/css"/>'."\r\n";
    	}
	} ?>
</head>
<body>
	<?php 
	// load navigation
	if(isset($nav) && $nav != null){
		$this->load->view($nav);
	} 
	
	// load page
	$this->load->view($page);
	
	// load javascript
	if(isset($js) && $js != null){
    	foreach ($js as $row) {
        	echo '<script src="'.$row.'" type="text/javascript"></script>'."\r\n";
    	}
	} ?>
<script type="text/javascript">
	$(document).ready(function(){
		<?php $name='hello';?>
		// $('input[name="<?php echo $name;?>"]').keyup(function(){});
		$('input[name="p0_start_1"]').keyup(function(){
			
		});
	});
</script>
</body>
</html>