-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 27, 2017 at 08:36 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asep`
--

-- --------------------------------------------------------

--
-- Table structure for table `obl`
--

CREATE TABLE `obl` (
  `obl_id` int(11) NOT NULL,
  `nama_cust` varchar(100) NOT NULL,
  `judul_kb` varchar(255) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `witel` varchar(20) NOT NULL,
  `mitra` varchar(20) NOT NULL,
  `nilai_cpe` float NOT NULL,
  `tipe_project_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `komite` varchar(10) NOT NULL,
  `p0_p1` int(11) NOT NULL,
  `p2_p6` int(11) NOT NULL,
  `kb` int(11) NOT NULL,
  `asuransi` varchar(6) NOT NULL,
  `p7_p8` int(11) NOT NULL,
  `kl` int(11) NOT NULL,
  `delivery` int(11) NOT NULL,
  `billcom` varchar(10) NOT NULL,
  `payment` float NOT NULL,
  `due_date` date NOT NULL,
  `progress` int(11) NOT NULL,
  `leftt` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `issue_obl_delivery` varchar(255) NOT NULL,
  `sudah_diakui` int(11) NOT NULL,
  `belum_diakui` int(11) NOT NULL,
  `product_spec` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obl`
--

INSERT INTO `obl` (`obl_id`, `nama_cust`, `judul_kb`, `priority_id`, `witel`, `mitra`, `nilai_cpe`, `tipe_project_id`, `start_date`, `komite`, `p0_p1`, `p2_p6`, `kb`, `asuransi`, `p7_p8`, `kl`, `delivery`, `billcom`, `payment`, `due_date`, `progress`, `leftt`, `total`, `issue_obl_delivery`, `sudah_diakui`, `belum_diakui`, `product_spec`) VALUES
(1, 'ALUMINDO CIPTA PERSADA ', 'PEKERJAAN ELEKTRIKAL PROYEK HARCO MANGGA BESAR', 4, 'JAKUT', 'INFOMEDIA', 10818, 1, '2016-09-20', '', 0, 0, 0, '', 0, 0, 0, '2/?', 0, '0000-00-00', 306, 0, 0, 'Penyelesaian Bank Garansi dan Adendum Kontrak ', 2825, 7994, ' Elektrikal, Grounding'),
(2, 'ALUMINDO CIPTA PERSADA ', 'PEKERJAAN ELEKTRONIK PROYEK HARCO MANGGA BESAR', 4, 'JAKUT', 'INFOMEDIA', 6210, 1, '2017-02-27', '', 0, 0, 0, '', 0, 0, 0, '1/?', 0, '0000-00-00', 146, 0, 0, 'infonya dipending sama pak teguh, atas perintah pak yoyok. Jadi harus sounding ke pak yoyok', 1584, 4626, ''),
(3, 'ANDOWA MEDIA SOLUSI ', 'PENYEDIAAN LAYANAN ASTINET LITE DAN TOWER MONOPOLE', 5, 'JAKTIM', 'PINS', 3636, 2, '2017-03-20', '0207\nP1', 0, 0, 0, 'JMDO', 0, 0, -42814, '0/2', 0, '2017-08-15', 125, 23, 148, 'Percepatan delivery. Pernah accrue kena reverse', 0, 0, ' Pembangunan tower monopole\nAstinet Lite 5 Mbps'),
(4, 'ANUGERAH TIGA PUTRI I', 'PENYEDIAAN SOLUSI SISTEM MONITORING DISTRIBUSI HASIL TAMBANG TAHAP 1', 5, 'JAKPUS', 'PINS', 6522, 2, '2016-12-31', '', 0, 0, 0, 'BDKR', 0, 0, 0, '', 0, '2017-09-13', 204, 0, 0, 'Dokumen sudah di sekdiv. Akan di accrue', 0, 0, ' Dump truck, eskavator, genset, gps. NON CPE: Astinet lite 10'),
(5, 'ARMINDO CATUR PRATAMA', 'SOLUSI MONITORING TOOLS TOWER PRODUCTION', 3, 'JAKSEL', 'PINS', 7247, 2, '2017-04-27', '', 0, 0, 0, 'BDKR', 0, 0, 0, '', 0, '0000-00-00', 87, 0, 0, 'sirkulir di bu rosna', 0, 0, 'Software, server, ups, dekstop, puch yangli, stamping, cliping'),
(6, 'ASAFA KABERA SINERGI', 'PENYEDIAAN PERANGKAT QUEING SYSTEM DI PLAZA SMARTFREN BOGOR', 1, 'BOGOR', 'INFOMEDIA', 2995, 2, '2017-02-14', '', 0, 0, 0, 'JMDO', 0, 0, 122, '2/2', 0, '0000-00-00', 159, 0, 0, 'Percepatan delivey', 330, 2665, 'TV LED; Bracket; dekstop; printer; ups; tablet; thermal paper; mini kios'),
(7, 'BAYU BUANA SEMESTA', 'PENYEDIAAN MEKANIKAL ELEKTRIKAL GEDUNG PARKIR DI PLAZA BALIKPAPAN', 1, 'JAKTIM', 'INFOMEDIA', 6565, 2, '2017-02-21', '', 0, 0, 0, 'JMDO', 0, 0, 120, '3/3', 0, '0000-00-00', 108, 0, 0, '', 0, 0, ' ventilasi, elektrical, fire alarm, tata suara, cctv, telepon, mep'),
(8, 'DWI MAS GEMILANG', '', 5, 'JAKBAR', 'MDM', 0, 3, '2017-07-14', '', -42930, 0, 0, '', 0, 0, 0, '', 0, '0000-00-00', 9, 0, 0, '', 0, 0, ''),
(9, 'GAPURA KENCANA ABADI', 'PEKERJAAN ELEKTRONIK PROYEK BOGOR ICON TOWER ALPINE', 4, 'BOGOR', 'PINS', 1650, 1, '2017-01-01', '', 0, 0, 0, '', 0, 0, 103, '1/?', 0, '0000-00-00', 203, 0, 0, '', 0, 100, 'fire alarm, tata suara, cctv, access control, matv'),
(10, 'GLOBAL OPTIMA ABADI', 'PENGADAAN FO', 6, 'BEKASI', 'INFOMEDIA', 4734, 2, '2017-06-07', '', 0, 0, 0, '', 0, 0, 0, '', 0, '0000-00-00', 46, 0, 0, 'Project dipecah per TREG', 0, 0, 'ADSS Cable; Span, Joint Box; ODF'),
(11, 'GRAHA RAYA ESTETIKA', 'INSTALASI CCTV UNTUK KAWASAN PERGUDANGAN KIRANA UTAMA', 4, 'BOGOR', 'INFOMEDIA', 909, 4, '2015-09-03', '', 0, 0, 0, '', 0, 0, 635, '', 0, '0000-00-00', 689, 0, 0, '', 0, 0, 'FO, CCTV, NVR, Monitor, Rack, Tiang'),
(12, 'INTERNUSA PERKASA TEKNIK', 'PENYEDIAAN SISTEM MONITORING INFRASTRUKTUR UNTUK DISTRIBUSI GAS', 5, 'JAKTIM', 'INFOMEDIA', 13000, 2, '2017-07-14', '', 0, 0, 0, '', 0, 0, 0, '', 0, '0000-00-00', 9, 0, 0, '', 0, 0, ''),
(13, 'JASA KAWAN TRANSPORTASI', 'TOTAL SERVICE SOLUTION TRANSPORTASI EKSPEDISI DALAM KOTA', 2, 'BOGOR', 'PINS', 6460, 3, '2017-05-22', '', 0, 0, 0, '', 0, 0, 0, '', 0, '0000-00-00', 62, 0, 0, 'Sirkuli Di witel', 0, 0, 'Dumpt truck HINO DUTRO 130MD-L ; GPS tracking'),
(14, 'KARYA SEMBADA CIPTA MANDIRI', 'ME UNTUK RS BRAWIJAYA SAWANGAN', 6, 'TANGERANG', 'MITRATEL', 6614, 2, '2017-05-29', '', -42939, 0, 0, '', 0, 0, 0, '', 0, '0000-00-00', 55, 0, 0, 'Belum ttd pak yoyok', 0, 0, 'Plumbing; Electrical; fire alarm; grounding; lighting'),
(15, 'KAYORI TISKI JAYA', 'NAVIGATION SISTEM KAPAL PERINTIS', 5, 'JAKSEL', 'MITRATEL', 1192, 2, '2017-03-23', '', 0, 0, 0, 'JMDO', 0, 0, 0, '', 0, '0000-00-00', 122, 0, 0, 'Ganti asurasni', 0, 0, 'Speaker; Radio; Marine Radar; Horn; astinet lite 10 mbps'),
(16, 'MAKMUR ANDAL BERSAMA', 'SOLUSI MONITORING DAN SISTEM CONTROL COLD STORAGE', 3, 'TANGERANG', 'INFOMEDIA', 19909, 2, '2017-05-08', '', -42939, 0, 0, '', 0, 0, 0, '', 0, '0000-00-00', 76, 0, 0, 'RAB belum dan menunggu ijin prinsip dari asuransi sinar mas. Spk dari bohir', 0, 0, 'Rack Galvanis; Railing; Table Processing; Condensing Unit'),
(17, 'NUSA KIRANA', 'PEKERJAAN ELEKTRONIKA PHASE 1', 4, 'JAKUT', 'TA', 23200, 1, '2015-01-01', '', 0, 0, 0, '', 0, 0, 934, '13/14', 0.948276, '0000-00-00', 934, 0, 0, '', 0, 0, 'ME'),
(18, 'PANTU PATALA ANDALAN', 'SMART MACHINE MONITORING SYSTEM', 3, 'BEKASI', 'PINS', 1592, 2, '2017-03-21', '', 0, 0, 0, 'BDKR', 0, 0, 0, 'ACR\n MAR', 0, '0000-00-00', 124, 0, 0, 'Amandemen KB', 0, 0, 'Mesin Printing; mesin potong; mesin jahit (jilid); mesin pond'),
(19, 'PERINA MEDIKA EDUTAMA', 'PENYEDIAAN LAYANAN CONNECTIVITY DAN SMART MEDICAL EQUIPMENT ', 4, 'TANGERANG', 'ADMEDIKA', 512, 2, '2017-03-31', '', 0, 0, 0, '', 0, 0, 40, 'ACR\n JUNI', 0, '0000-00-00', 114, 0, 0, 'Delivery setelah lebaran', 0, 500, 'USG, Laptop, TV soni, printer, OS, Projector'),
(20, 'PIRANTI CITRA MUSTIKA', 'PENYEDIAAN SOLUSI SMART OFFICE EQUIPMENT DAN GADGET ', 1, 'JAKSEL', 'SIGMA', 20719, 2, '2017-03-22', '', 0, 0, 0, 'BDKR', 0, 0, 0, '', 0.61373, '0000-00-00', 31, 0, 0, 'Menunggu pembayaran. Pembayaran sudah 8M', 0, 0, 'Toner; Server; ATK, PC\n'),
(21, 'SARANA DOA BERSAMA', 'SOLUSI SHIPPING MANAGEMENT SYSTEM', 4, 'BEKASI', 'PINS', 34432, 3, '2017-02-21', '', 0, 0, 0, 'BDKR', 0, 0, 0, 'ACR\nMEI', 0, '2017-08-30', 152, 38, 190, 'Sudah PO ke dealer. Bulan agustus pekerjaan akan selesai', 0, 0, 'Dump truck 30 unit'),
(22, 'SHIMIZU - TOTAL BANGUN PERSADA JOINT OPERATION ', 'PEKERJAAN MOBILE BOOSTER PROYEK MENARA ASTRA ', 4, 'JAKBAR', 'MITRATEL', 8760, 1, '2017-02-14', '', 0, 0, 0, '', 0, 0, 0, '1/?', 0, '0000-00-00', 159, 0, 0, 'KB dual bahasa deadline 14 juli', 407, 8353, ' IBS (Kabel feeder, antena, amplifier)'),
(23, 'SUPLAINDO SEJAHTERA', 'SMART OFFSET PRINTING MACHINE', 1, 'JAKUT', 'SIGMA', 21718, 2, '2017-02-09', '', 0, 0, 0, 'BDKR', 0, 0, 0, '', 0, '0000-00-00', 95, 0, 0, '', 21000, 0, 'Printer'),
(24, 'TANI SEJAHTERA', 'PENYEDIAN DISTRIBUTION MONITORING SYSTEM HASIL PERTANIAN TEBU', 2, 'BOGOR', 'PINS', 2266, 3, '2017-06-06', '', 0, 0, 0, '', 0, 0, 0, '', 0, '0000-00-00', 47, 0, 0, 'Sirkulir di witel', 0, 0, 'Dumpt truck colt diest mitsubishi FE 74 HD; GPS tracking'),
(25, 'TRIASA ELANG NUSA', 'PENYEDIAAN FMS FOR HEAVY LOAD TRANSPORTATION MENGGUNAKAN SCS', 5, 'JAKSEL', 'PINS', 23428, 3, '2017-02-21', '', 0, 0, 0, 'BDKR', 0, 0, 0, '', 0, '0000-00-00', 152, 0, 0, 'Cetak ulang asuransi dan ganti judul di asuransi. ', 0, 0, ' Dump Truck,  Fleet Management System'),
(26, 'ASEP SURYA', 'BIGO LIVE', 4, 'SERENA', 'INDIBIZNET', 123, 2, '2017-07-17', '', 0, 0, 0, '', 0, 0, 0, '', 0, '2017-08-31', 0, 2131, 4131, 'ANJIR TAI KUCING', 0, 1, 'BODO AMAT'),
(27, 'ASEP SURYA', 'TAI', 6, 'TAI', 'MITI', 1231, 4, '2017-07-20', '', 0, 0, 0, '', 0, 0, 0, '', 0, '2017-07-31', 0, 12, 2342, 'ANJIR TAI KUCING', 0, 1, 'BODO AMAT');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `priority_id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`priority_id`, `nama`) VALUES
(1, 'LOWER'),
(2, 'LOW'),
(3, 'MEDIUM'),
(4, 'HIGH'),
(5, 'HIGHER'),
(6, 'DO');

-- --------------------------------------------------------

--
-- Table structure for table `time_data`
--

CREATE TABLE `time_data` (
  `time_data_id` int(11) NOT NULL,
  `obl_id` int(11) NOT NULL,
  `p0_start` date DEFAULT NULL,
  `p0_end` date DEFAULT NULL,
  `p2_start` date DEFAULT NULL,
  `p2_end` date DEFAULT NULL,
  `p7_start` date DEFAULT NULL,
  `p7_end` date DEFAULT NULL,
  `kb_start` date DEFAULT NULL,
  `kb_end` date DEFAULT NULL,
  `kl_start` date DEFAULT NULL,
  `kl_end` date DEFAULT NULL,
  `asn_start` date DEFAULT NULL,
  `asn_end` date DEFAULT NULL,
  `del_start` date DEFAULT NULL,
  `del_end` date DEFAULT NULL,
  `asu_start` date DEFAULT NULL,
  `asu_end` date DEFAULT NULL,
  `komi_start` date DEFAULT NULL,
  `komi_end` date DEFAULT NULL,
  `bil_start` date DEFAULT NULL,
  `bil_end` date DEFAULT NULL,
  `pay_piutang` int(11) DEFAULT NULL,
  `pay_pembayaran` int(11) DEFAULT NULL,
  `pay_sisa` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `time_data`
--

INSERT INTO `time_data` (`time_data_id`, `obl_id`, `p0_start`, `p0_end`, `p2_start`, `p2_end`, `p7_start`, `p7_end`, `kb_start`, `kb_end`, `kl_start`, `kl_end`, `asn_start`, `asn_end`, `del_start`, `del_end`, `asu_start`, `asu_end`, `komi_start`, `komi_end`, `bil_start`, `bil_end`, `pay_piutang`, `pay_pembayaran`, `pay_sisa`) VALUES
(1, 1, '2017-07-18', NULL, '2001-03-13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 908793, 872231, -36562),
(2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 3, '2017-07-13', '2017-08-14', '2017-07-19', NULL, '2017-07-26', NULL, '2017-07-03', NULL, '2017-07-29', NULL, '2017-07-25', NULL, '2017-07-17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 5, NULL, NULL, NULL, NULL, NULL, NULL, '2019-02-10', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tipe_project`
--

CREATE TABLE `tipe_project` (
  `tipe_project_id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_project`
--

INSERT INTO `tipe_project` (`tipe_project_id`, `nama`) VALUES
(1, 'BIDDING'),
(2, 'REFINANCING'),
(3, 'ACQUITION'),
(4, 'DEVICE');

-- --------------------------------------------------------

--
-- Table structure for table `utang`
--

CREATE TABLE `utang` (
  `utang_id` int(11) NOT NULL,
  `obl_id` int(11) NOT NULL,
  `tipe` varchar(15) NOT NULL,
  `value` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utang`
--

INSERT INTO `utang` (`utang_id`, `obl_id`, `tipe`, `value`, `timestamp`) VALUES
(1, 1, 'utang', 908793, '2017-07-27 01:14:29'),
(2, 1, 'bayar', 872231, '2017-07-27 01:15:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `obl`
--
ALTER TABLE `obl`
  ADD PRIMARY KEY (`obl_id`),
  ADD KEY `priority_id` (`priority_id`),
  ADD KEY `tipe_project` (`tipe_project_id`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`priority_id`);

--
-- Indexes for table `time_data`
--
ALTER TABLE `time_data`
  ADD PRIMARY KEY (`time_data_id`),
  ADD UNIQUE KEY `obl_id` (`obl_id`);

--
-- Indexes for table `tipe_project`
--
ALTER TABLE `tipe_project`
  ADD PRIMARY KEY (`tipe_project_id`);

--
-- Indexes for table `utang`
--
ALTER TABLE `utang`
  ADD PRIMARY KEY (`utang_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `obl`
--
ALTER TABLE `obl`
  MODIFY `obl_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `priority_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `time_data`
--
ALTER TABLE `time_data`
  MODIFY `time_data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tipe_project`
--
ALTER TABLE `tipe_project`
  MODIFY `tipe_project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `utang`
--
ALTER TABLE `utang`
  MODIFY `utang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `obl`
--
ALTER TABLE `obl`
  ADD CONSTRAINT `obl_ibfk_1` FOREIGN KEY (`tipe_project_id`) REFERENCES `tipe_project` (`tipe_project_id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `obl_ibfk_2` FOREIGN KEY (`priority_id`) REFERENCES `priority` (`priority_id`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
